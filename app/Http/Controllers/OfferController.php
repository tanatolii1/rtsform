<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class OfferController extends Controller
{
    public function offers(Request $request)
    {
        $username = $request->input('username');
        $email = $request->input('email');
        $message = $request->input('message');
        $file = $request->file('file')->store('offers','public');
        $path = $request->file('file');

        $genName = $request->file->store('offers');

        $FileName = $path->getClientOriginalName();

        $textfile = '[{"download_link":"'.$genName.'","original_name":"'.$FileName.'"}]';

        DB::table('offers')->insert(
            ['name' => $username, 'cv' => $textfile, 'email' => $email, 'message' => $message, 'created_at'=> NOW() ]
        );
    }
}
