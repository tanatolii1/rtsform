@extends('layouts.app')

@section('content')
    <div class="col-3 block row col-md-3 offset-md-4 justify-content-center ">
        <div class="offer ">
            Job offer
        </div>
        <div class="descrioption ">
            if u want join our team please send your CV.
        </div>
    <form action="{{route('formoffer')}}" method="post" enctype="multipart/form-data">

        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <label for="username">Your Name</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
            </div>
            <input type="username" class="form-control" name="username" placeholder="Username" >
        </div>

        <label for="file">Your CV</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-envelope-open-text"></i></span>
            </div>
            <input type="file" class="form-control-file form-control" name="file" id="file">
        </div>

        <label for="email">Your Email Address</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
            </div>
            <input type="email" class="form-control" id="email" name="email" placeholder="example office@example.com">
        </div>


        <label for="message">Your Message</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend hh">
                <span class="input-group-text message"><i class="fas fa-comment-alt"></i></span>

            </div>

            <textarea class="form-control" rows="6" id="message" name="message" placeholder="example I'd like to say &#34;Hallo world!&#34;"></textarea>
        </div>




        <button type="submit"  class="btn btn-primary btn-block  btn-lg">Submit your CV</button>
    </form>
    </div>